# PostgresNode Next Generation

This is a version of PostgresNode that leverages the new multi-install path
and version awareness of PostgresNode to make it as fully compatible
as possible with older versions of Postgres. It is tested using the binaries
found at [pg-old-bin](https://gitlab.com/adunstan/pg-old-bin)

Usage:

```perl
use lib '/path/to/postgres/src/test/perl'; # for TestLib etc.
use lib '/path/to/here'; # for PostgresNode and PostgresVersion

use PostgresNode;

my $node = get_new_node('node_name', install_path => '/path/to/binaries');

$node->init;

...

```

`testpgnode.pl` is the test script used for testing the facility.


